<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

 function download_sheet($sp_key) {
 	$url = "https://spreadsheets.google.com/feeds/cells/{$sp_key}/public/basic?alt=json";
 	$str = file_get_contents($url);
         $data = json_decode($str, true);
         $id_marker = "https://spreadsheets.google.com/feeds/cells/{$sp_key}/public/basic/";
         $entries   = $data["feed"]["entry"];

         $res = array();
         foreach($entries as $entry) {
            $content = $entry["content"];
            $ind = str_replace($id_marker."R", "", $entry["id"]['$t']);
            $ii  = explode("C", $ind);
            $res[$ii[0]-1][$ii[1]-1] = $entry["content"]['$t'];
         }

         return $res;
     }
//https://docs.google.com/spreadsheets/d/1bZQKtNUHVSAyNIE4h7dZc6B4moEG-uB0p_jvlgWbQ_U/edit?usp=sharing
//https://docs.google.com/spreadsheets/d/e/2PACX-1vRvWFA6I7-pWdLqoTjc6GI_VGc-YypDKJLgBU73sLLRHE0cJ10gTwNo1xBR4gFLqYhegp9SWXlWZ1KM/pubhtml
 $SP_key = "1bZQKtNUHVSAyNIE4h7dZc6B4moEG-uB0p_jvlgWbQ_U/1";
 $rawdata = download_sheet($SP_key);
 $data1 = json_encode($rawdata);
 //echo $data1;
 $raw1 = json_decode($data1,true);
 //$data1 = json_encode($raw1,true);
 $temp_products = array();
 $products = array();
 $products2 = array();
 if (is_array($raw1) || is_object($raw1))
 {
    foreach ($raw1 as $productarr ) {
      $products2['sku'] = (isset($productarr[0])? $productarr[0]:'');// => 'wsd001'
      $products2['parentsku']  = (isset($productarr[1])? $productarr[1]:'');//=> ''
      $products2['producttype']  = (isset($productarr[2])? $productarr[2]:'');//=> 'Parent'
      $products2['category']  = (isset($productarr[3])? $productarr[3]:'');//=> 'Dresses'
      $products2['name']  =  (isset($productarr[4])? $productarr[4]:'');//=> 'Lafayette Convertible Dress'
      $products2['description']  = (isset($productarr[5])? $productarr[5]:'');//=> 'This all day dress has a flattering silhouette and a convertible neckline to suit your mood. Wear tied and tucked in a sailor knot, or reverse it for a high tied feminine bow.'
      $products2['additionaldescription']  = (isset($productarr[6])? $productarr[6]:'');//=> 'Two sash, convertible neckline with front ruffle detail. Unhemmed, visisble seams. Hidden side zipper. Unlined. Wool/elastane. Hand wash.'
      $products2['price']  = (isset($productarr[7])? $productarr[7]:'');//=> '349.00'
      $products2['color']  = (isset($productarr[8])? $productarr[8]:'');//=> 'Blue'
      $products2['size']  = (isset($productarr[9])? $productarr[9]:'');//=> ''
      $products2['imageurl']  = (isset($productarr[10])? $productarr[10]:'');//=>' http://demostore.landedcost.io/media/catalog/product/cache/1/small_image/260x/9df78eab33525d08d6e5fb8d27136e95/w/s/wsd013t.jpg'
      if ($productarr[2] !== 'producttype'){
       $products[] = $products2;
     }
    }
  }

$language 		= get_language();
//$translations 	= $cache->get("translations_cache_".$language);

$SP_key = "1bZQKtNUHVSAyNIE4h7dZc6B4moEG-uB0p_jvlgWbQ_U/3";
$rawdata = download_sheet($SP_key);
$data1 = json_encode($rawdata);
$raw1 = json_decode($data1);
//$data1 = json_encode($raw1,true);
$translations = array();
if (is_array($raw1) || is_object($raw1))
 {
   foreach ($raw1 as list($a,$b)) {
       if($a !== 'key'){
        $translations[$a] = $b;
       }
   }
 }

 $SP_key = "1bZQKtNUHVSAyNIE4h7dZc6B4moEG-uB0p_jvlgWbQ_U/2";
 $rawdata = download_sheet($SP_key);
 $data1 = json_encode($rawdata);
 //echo $data1;
 $raw1 = json_decode($data1,true);
 //$data1 = json_encode($raw1,true);
 $temp_products = array();
 $products = array();
 $products2 = array();
 $productscat = array();
 $productscol = array();
 $categories 		= array();
 $product_types 		= array();
 $colors				= array();
 if (is_array($raw1) || is_object($raw1))
  {
    foreach ($raw1 as $productarr ) {
      $products2['categories'] = (isset($productarr[0])? $productarr[0]:'');// => 'wsd001'
      $products2['product_type']  = (isset($productarr[1])? $productarr[1]:'');//=> ''
      $products2['colors']  = (isset($productarr[2])? $productarr[2]:'');//=> 'Parent'
      $productscat[$productarr[0]] = (isset($productarr[0])? $productarr[0]:'');// => 'wsd001'
      $productscol[]  = (isset($productarr[2])? $productarr[2]:'');//=> 'Parent'
      if ($productarr[0] !== 'categories'){
       $types[] = $products2;
       $categories = $productscat;
       $colors = $productscol;
     }
    }
  }

  $types['categories'] 		= $categories;
  $types['product_types'] 	= $product_types ;
  $types['colors'] 			= $colors;


function _m($key) {
	global $store_config;
	if (isset($store_config[$key])) {
		return $store_config[$key];
	} else {
		return $key;
	}
}


function _l($key) {
	global $translations;
	if (isset($translations[$key])) {
		return $translations[$key];
	} else {
		return $key;
	}

}

function get_language() {
	$lang 	= 'en';
	$domain = explode('-',$_SERVER['SERVER_NAME']);

	if (isset($domain[1])) {
		$parts 	= explode('.',$domain[1]);
		$lang 	= $parts[0];
	}

	return $lang;
}
?>

<!doctype html>
<html ng-app="YourStore">
  <head>
    <title><?php echo _l('store_title');?></title>
    <meta name="description" content="<?php echo _l('store_description');?>">
    <meta name="keywords" content="<?php echo _l('store_keywords');?>">
    <meta name="author" content="<?php echo _l('store_author');?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <!-- jQuery, Angular -->
    <script src="http://code.jquery.com/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.3.6/angular.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.3.6/angular-route.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.3.6/angular-animate.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.3.6/angular-aria.min.js"></script>
     <script src="http://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.13.0.js"></script>
    <link href='http://fonts.googleapis.com/css?family=Dosis:200,300,400,500,600,700,800' rel='stylesheet' type='text/css'/>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,700,600,300' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <!-- AngularStore app - >
    <script src="js/list/modernizr.custom.js" type="text/javascript"></script>
    <script src="js/app/index.js" type="text/javascript"></script>
    <script src="js/cart/index.js" type="text/javascript"></script>
    <script src="js/controller/index.js" type="text/javascript"></script>
    <!---->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link href="css/component.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="css/style_cube.css"/>
  </head>
  <body>
      <div class="container">
        <div class="container-shadow">
            <a class="logo" href="#/store">
              <img src="<?php echo _m('store_logo');?>"  alt="logo" height="44px"/>
            </a>
          <h1 class="well">
              <?php echo _l('store_title');?>
          </h1>
          <div ng-view ></div>
          <div class="content-footer">
            <div class="col-md-12" style="background:#fff;padding:20px;text-align:center;">
            		<p><?php echo _l('copyright');?> <?php echo date('Y'); ?> <?php echo _l('store_title');?> <?php echo _l('rights_reserved');?></p>
            		<p><a href='https://onlineshop-gae.appspot.com/partials/customerService'><?php echo _l('customer_service');?></a></p>
            </div>
          </div>
         </div>
      </div>
  </body>
</html>
